const browserSync = require("browser-sync").create(),
	cachebust = require("gulp-cache-bust"),
	clean = require("gulp-clean"),
	cleanCss = require("gulp-clean-css"),
	cssnext = require("postcss-cssnext"),
	concat = require("gulp-concat"),
	ghPages = require("gh-pages"),
	gulp = require("gulp"),
	gutil = require("gulp-util"),
	postcss = require("gulp-postcss"),
	sourcemaps = require("gulp-sourcemaps"),
	webpack = require("webpack"),
	webpackConfig = require("./webpack.config.js"),
	webpackConfigProduction = require("./webpack.config.production.js");

function map_error(err) {
	gutil.log(gutil.colors.red(err.name) +
		": " +
		gutil.colors.yellow(err.message));

  this.emit("end");
}

function onBuild(done) {
	return function(err, stats) {
		if (err) {
			gutil.log("Error", err);
		} else if (stats.compilation.errors.length) {
			stats.compilation.errors.forEach(compErr => {
				gutil.log(gutil.colors.yellow(compErr));
			});
		}
		done();
	};
}

function getStyle(optimize) {
	const plugins = [
		cssnext({ browsers: ["last 2 versions"] })
	];

	let stream = gulp.src([
			"./src/style/**/*.css"
		]);

	if (!optimize) {
		stream = stream.pipe(sourcemaps.init());
	}

	stream = stream
		.pipe(concat("bundle.css"))
		.pipe(postcss(plugins))
		.on("error", map_error);

	if (optimize) {
		stream = stream
			.pipe(cleanCss())
			.on("error", map_error);
	} else {
		stream = stream.pipe(sourcemaps.write("."));
	}

	return stream;
}

gulp.task("build-script", (done) => {
	webpack(webpackConfig).run(onBuild(() => {
		browserSync.reload();
		done();
	}));
});

gulp.task("build-style", () => {
	return getStyle(false)
		.pipe(gulp.dest("./target/build"))
		.pipe(browserSync.stream({ match: "**/*.css" }));
});

gulp.task("build-static", () => {
	return gulp.src("./src/static/**/*")
		.pipe(gulp.dest("./target/build"))
		.on("end", browserSync.reload);
});

gulp.task("clean-build", () => {
	return gulp.src("./target/build/*", { read: false })
		.pipe(clean());
});

gulp.task("clean-release", () => {
	return gulp.src("./target/release/*", { read: false })
		.pipe(clean());
});

gulp.task("release-script", (done) => {
	webpack(webpackConfigProduction).run(onBuild(() => {
		done();
	}));
});

gulp.task("release-style", () => {
	return getStyle(true)
		.pipe(gulp.dest("./target/release"));
});

gulp.task("release-static", () => {
	return gulp.src("./src/static/**/*")
		.pipe(gulp.dest("./target/release"));
});

gulp.task("release-cachebust", () => {
	return gulp.src("./target/release/index.html")
		.pipe(cachebust())
		.pipe(gulp.dest("./target/release"));
});

gulp.task("gh-publish", (done) => {
	ghPages.publish("./target/release", done);
});

gulp.task("browser-sync", () => {
	browserSync.init({
		ghostMode: false,
		notify: false,
		open: false,
		reloadDebounce: 2000,
		server: {
			baseDir: "./target/build"
		},
		ui: false
	});
});

gulp.task("watch", () => {
	gulp.watch("./src/static/**/*", gulp.series("build-static"));
	gulp.watch("./src/style/**/*.css", gulp.series("build-style"));
	gulp.watch([
		"./src/script/**/*",
		"./src/tmpl/**/*"
	], gulp.series("build-script"));
});

gulp.task("build", gulp.series("clean-build", "build-script", "build-style", "build-static"));

gulp.task("release", gulp.series("clean-release", "release-script", "release-style", "release-static", "release-cachebust"));

gulp.task("deploy", gulp.series("release", "gh-publish"));

gulp.task("dev-server", gulp.series("build", gulp.parallel("browser-sync", "watch")));
