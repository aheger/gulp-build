# gulp-build
Build environment with gulp, babel, cssnext and dev-server

```npm install```

Install required packages

```npm start```

start dev-server

```npm run build```

create build in ./target/build

```npm run release```

create release build in ./target/release
