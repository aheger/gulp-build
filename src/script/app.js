import tmpl from "../tmpl/app.html";

export default () => {
	let el = document.createElement("p");

	el.innerHTML = tmpl;

	return el;
};
