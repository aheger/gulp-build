const path = require("path"),
	{ EnvironmentPlugin } = require("webpack");

module.exports = function() {
	return {
		entry: "./src/script/index.js",
		output: {
			path: path.join(__dirname, "target/build"),
			filename: "bundle.js",
			chunkFilename: "[id].bundle.js",
			sourceMapFilename: "[file].map",
			pathinfo: false
		},
		module: {
			rules: [
				{
					test: /\.js$/,
					exclude: /node_modules/,
					use: "babel-loader"
				},
				{
					test: /\.(html)$/,
					use: "html-loader"
				}
			]
		},
		plugins: [
			new EnvironmentPlugin(["NODE_ENV"])
		]
	};
};
