const conf = require("./webpack.config.base.js")();

conf.devtool = "source-map";

module.exports = conf;
