const path = require("path"),
	{ optimize: { UglifyJsPlugin } } = require("webpack"),
	conf = require("./webpack.config.base.js")();

conf.plugins.push(new UglifyJsPlugin());

conf.output.path = path.join(__dirname, "./target/release");

module.exports = conf;
